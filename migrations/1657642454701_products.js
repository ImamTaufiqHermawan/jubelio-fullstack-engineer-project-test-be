exports.up = (pgm) => {
  pgm.createTable('products', {
    id: 'id',
    name: { type: 'varchar(1000)', notNull: true },
    sku: { type: 'varchar(1000)', notNull: true, unique: true },
    image: { type: 'text', notNull: false },
    price: { type: 'integer', notNull: true },
    description: { type: 'text', notNull: false },
    createdAt: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp'),
    },
    updatedAt: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp'),
    },
  })
}