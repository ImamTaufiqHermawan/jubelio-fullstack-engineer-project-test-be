require('dotenv').config();
const Hapi = require("hapi");
const HapiPostgresConnection = require('hapi-postgres-connection');
const axios = require('axios');
const Joi = require("joi");

const parse = require('./utils/parser');
const filterData = require('./utils/filter');

const init = async () => {

  // Init Server
  const server = Hapi.Server({
    port: 3000,
    host: 'localhost',
    routes: {
      cors: true
    }
  });

  // Start Server
  await server.start();
  console.log('Server running on %s', server.info.uri);

  // Init Database Server
  await server.register({
    plugin: HapiPostgresConnection
  });

  // API to import product from Elevania API
  server.route({
    method: 'GET',
    path: '/api/products/import',
    handler: async function (request, h) {
      // Hit API Elevania utk get data
      try {
        const result = await axios.get(
          'http://api.elevenia.co.id/rest/prodservices/product/listing',
          {
            headers: {
              openapikey: process.env.OPENAPIKEY,
            },
          }
        )

        // Parse response data dari Elevania yg XML ke JSON
        const parserData = await parse(result.data)
        const data = parserData.Products.product

        // Filter data utk di insert ke DB
        const filteredData = await filterData(data)

        // Import Data ke DB
        for (let i = 0; i < filteredData.length; i++) {
          const query = {
            text: 'INSERT INTO products(name, price, sku) VALUES($1, $2, $3)',
            values: [filteredData[i].name, filteredData[i].price, filteredData[i].SKU],
          }
          await request.pg.client.query(query);
        }

        return h.response({ ok: true, message: 'success import data' }).code(201);
      } catch (err) {
        return h.response({ ok: false, message: err.message }).code(400);
      }
    }
  });

  // API to get all products from DB
  server.route({
    method: 'GET',
    path: '/api/products',
    handler: async function (request, h) {
      let select = `SELECT * FROM products`;

      try {
        const result = await request.pg.client.query(select);
        return h.response(result.rows);
      } catch (err) {
        return h.response({ ok: false, message: err.message }).code(400);
      }
    }
  });

  // API to get product by id
  server.route({
    method: 'GET',
    path: '/api/products/{id}',
    handler: async function (request, h) {
      const id = request.params.id;

      try {
        const result = await request.pg.client.query('SELECT * FROM products WHERE id = $1', [id])
        return h.response(result.rows[0]);
      } catch (err) {
        return h.response({ ok: false, message: err.message }).code(400);
      }

    },
    // options: {
    //   validate: {
    //     params: {
    //       id: Joi.number().integer()
    //     }
    //   }
    // }
  });

  // API to create new product
  server.route({
    method: 'POST',
    path: '/api/products',
    handler: async function (request, h) {
      try {
        const { name, price, SKU, description } = request.payload
        console.log(name)

        const query = {
          text: 'INSERT INTO products(name, price, sku, description) VALUES($1, $2, $3, $4)',
          values: [name, price, SKU, description],
        }
        await request.pg.client.query(query);

        return h.response({ ok: true, message: `Created product with name ${name}` }).code(201)
      } catch (err) {
        return h.response({ ok: false, message: err.message }).code(400);
      }
    },
    // options: {
    //   validate: {
    //     payload: {
    //       username: Joi.string().alphanum().min(3).max(30).required(),
    //       email: Joi.string().email(),
    //       password: Joi.string().regex(/^[a-zA-Z0-9]{8,30}$/)
    //     }
    //   }

    // }
  });

  // API to delete existing product
  server.route({
    method: 'DELETE',
    path: '/api/products/{id}',
    handler: async function (request, h) {
      const id = request.params.id;

      try {
        const result = await request.pg.client.query('DELETE FROM products WHERE id = $1', [id]);
        return h.response(result)
      } catch (err) {
        return h.response({ ok: false, message: err.message }).code(400);
      }
    },
    // config: {
    //   validate: {
    //     params: {
    //       id: Joi.number().integer(),
    //       mid: Joi.number().integer()
    //     }
    //   }
    // }
  });
};

process.on('unhandledRejection', (err) => {

  console.log(err);
  process.exit(1);
});

init();

// TODO
// 1. root cause knp Joi validation error kena invalid schema
// 2. refactor code
