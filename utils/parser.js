// function untuk parse xml ke json
const xml2js = require('xml2js');

async function parse(data) {
  const promise = await new Promise((resolve, reject) => {
    const parser = new xml2js.Parser({ explicitArray: false });

    parser.parseString(data, (error, result) => {
      if (error) reject(error);
      else resolve(result);
    });
  });
  return promise;
}

module.exports = parse;
