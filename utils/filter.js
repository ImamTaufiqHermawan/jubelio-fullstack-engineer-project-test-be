
// function utk filter data
async function filterData(data) {
  const filteredData = await data.map(item => {
    // console.log(item)
    const container = {};

    container.name = item.prdNm;
    container.price = item.selPrc;
    container.SKU = item.prdNo;

    return container;
  })
  return filteredData;
}

module.exports = filterData;
